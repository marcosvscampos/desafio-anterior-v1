package br.com.builders.treinamento.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;

public class Customer {
    @Id
    @JsonIgnore
    public ObjectId _id;

//    @NotNull
//    @NotBlank
    private String id;
//    @NotNull
    private String crmId;
//    @Pattern(regexp = "^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]",
//            message = "Invalid URL Format")
    private String baseUrl;
//    @NotNull
//    @Size(min = 5, message = "Name should not have less than 5 characters")
    private String name;
//    @Pattern(regexp="^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\\\.[A-Z]{2,6}$",
//            message = "Invalid Email Format")
    private String login;

    public Customer(){

    }

    public Customer(String id, String crmId, String baseUrl, String name, String login) {
        this.id = id;
        this.crmId = crmId;
        this.baseUrl = baseUrl;
        this.name = name;
        this.login = login;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCrmId() {
        return crmId;
    }

    public void setCrmId(String crmId) {
        this.crmId = crmId;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
