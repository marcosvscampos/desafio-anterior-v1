package br.com.builders.treinamento.utils;

import br.com.builders.treinamento.builders.CustomerResponseBuilder;
import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class APIResponseUtils {

    public static ResponseEntity generateSuccessStatus(List<Customer> list){
        return ResponseEntity.status(200).body(list);
    }

    public static ResponseEntity generateOkOrCreatedStatus(String id, String message, Integer status){
        CustomerResponse cr = new CustomerResponseBuilder().with($ -> {
            $.id = id;
            $.message = message;
        }).build();
        return ResponseEntity.status(status).body(cr);
    }
}
