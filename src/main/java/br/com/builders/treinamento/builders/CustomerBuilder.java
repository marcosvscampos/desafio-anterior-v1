package br.com.builders.treinamento.builders;

import br.com.builders.treinamento.domain.Customer;

import java.util.function.Consumer;

public class CustomerBuilder {

    public String id;
    public String crmId;
    public String baseUrl;
    public String name;
    public String login;

    public CustomerBuilder with(Consumer<CustomerBuilder> builderFunction){
        builderFunction.accept(this);
        return this;
    }

    public Customer build(){
        return new Customer(id, crmId, baseUrl, name, login);
    }

}
