package br.com.builders.treinamento.builders;

import br.com.builders.treinamento.dto.response.CustomerResponse;

import java.util.function.Consumer;

public class CustomerResponseBuilder {

    public String id;
    public String message;

    public CustomerResponseBuilder with(Consumer<CustomerResponseBuilder> builderFunction){
        builderFunction.accept(this);
        return this;
    }

    public CustomerResponse build(){
        return new CustomerResponse(id, message);
    }
}
