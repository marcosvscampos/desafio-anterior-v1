package br.com.builders.treinamento.repository;

import br.com.builders.treinamento.domain.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends MongoRepository <Customer, String> {

    Customer findById(String id);
}
