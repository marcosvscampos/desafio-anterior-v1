package br.com.builders.treinamento.resources;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.exception.NotFoundException;
import br.com.builders.treinamento.service.interfaces.ICustomerService;
import br.com.builders.treinamento.utils.APIResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Objects;


@RestController
@RequestMapping("/api/customers")
public class CustomerResource {

    @Autowired
    private ICustomerService _customerService;

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<List<Customer>> listAllCustomers(){
        List<Customer> customers = _customerService.findAll();
        return APIResponseUtils.generateSuccessStatus(customers);
    }

    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public ResponseEntity createCustomer(@RequestBody @Valid CustomerRequest cr){

        _customerService.save(cr);

        return APIResponseUtils.generateOkOrCreatedStatus(cr.getId(), "Item created", 201);
    }

    @RequestMapping(value="/{id}", method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public ResponseEntity updateCustomer(@PathVariable(value = "id") String id,
                                         @RequestBody @Valid CustomerRequest cr) throws NotFoundException{
        Customer ct = _customerService.findById(id);
        if(Objects.isNull(ct)){
            throw new NotFoundException("The customer (or any of supplied IDs) is not found");
        }

        _customerService.update(ct, cr);
        return APIResponseUtils.generateOkOrCreatedStatus(id, "Item replaced", 200);
    }

    @RequestMapping(value="/{id}", method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON, consumes = MediaType.APPLICATION_JSON)
    public ResponseEntity deleteCustomer(@PathVariable(value = "id") String id) throws NotFoundException {
        Customer ct = _customerService.findById(id);
        if(Objects.isNull(ct)){
            throw new NotFoundException("The customer (or any of supplied IDs) is not found");
        }
        _customerService.delete(ct);
        return APIResponseUtils.generateOkOrCreatedStatus(id, "Item deleted", 200);
    }

}
