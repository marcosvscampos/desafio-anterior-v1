package br.com.builders.treinamento.dto.response;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class CustomerResponse implements Serializable {

    private String id;
    private String message;

    public CustomerResponse(){
        super();
    }

    public CustomerResponse(String id, String message){
        this.id = id;
        this.message = message;
    }

    public String getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }
}
