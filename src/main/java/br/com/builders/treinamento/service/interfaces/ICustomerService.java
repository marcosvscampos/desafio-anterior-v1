package br.com.builders.treinamento.service.interfaces;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;

import java.util.List;

public interface ICustomerService {

    void save(CustomerRequest cr);

    List<Customer> findAll();

    Customer findById(String id);

    void update(Customer ct, CustomerRequest cr);

    void delete(Customer ct);
}
