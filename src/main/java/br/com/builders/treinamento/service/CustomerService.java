package br.com.builders.treinamento.service;

import br.com.builders.treinamento.builders.CustomerBuilder;
import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.request.CustomerRequest;
import br.com.builders.treinamento.repository.CustomerRepository;
import br.com.builders.treinamento.service.interfaces.ICustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerService implements ICustomerService {

    @Autowired
    private CustomerRepository repository;

    @Override
    public void save(CustomerRequest cr){
        Customer ct = new CustomerBuilder().with($ ->{
            $.id = cr.getId();
            $.crmId = cr.getCrmId();
            $.baseUrl = cr.getBaseUrl();
            $.login = cr.getLogin();
            $.name = cr.getName();
        }).build();
        repository.save(ct);
    }

    @Override
    public List<Customer> findAll(){
        return repository.findAll();
    }

    @Override
    public Customer findById(String id){
        return repository.findById(id);
    }

    @Override
    public void update(Customer ct, CustomerRequest cr){

        ct.setId(cr.getId());
        ct.setCrmId(cr.getCrmId());
        ct.setBaseUrl(cr.getBaseUrl());
        ct.setLogin(cr.getLogin());
        ct.setName(cr.getName());

        repository.save(ct);

    }

    @Override
    public void delete(Customer ct){
        repository.delete(ct);
    }
}
