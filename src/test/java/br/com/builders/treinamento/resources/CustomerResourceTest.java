package br.com.builders.treinamento.resources;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import br.com.builders.treinamento.utils.TestUtils;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
@WebAppConfiguration
public class CustomerResourceTest {

    protected MockMvc restMvc;

    protected TestUtils testUtils = new TestUtils();

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);

        CustomerResource customerResource = new CustomerResource();
        this.restMvc = MockMvcBuilders.standaloneSetup(customerResource).build();
    }

    @Test
    public void createCustomerSuccessfullyTest() throws Exception{
        MvcResult postResult = restMvc.perform(post("/customers").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.SC_CREATED)).andReturn();

        CustomerResponse cr = (CustomerResponse) testUtils.generateResponse(postResult);
        Assert.assertEquals(cr.getId(), "id");
        Assert.assertEquals(cr.getMessage(), "Item created");
    }

    @Test
    public void updateCustomerSuccessfullyTest() throws Exception{
        MvcResult postResult = restMvc.perform(put("/customers").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.SC_OK)).andReturn();

        CustomerResponse cr = (CustomerResponse) testUtils.generateResponse(postResult);
        Assert.assertEquals(cr.getId(), "id");
        Assert.assertEquals(cr.getMessage(), "Item replaced");
    }

    @Test
    public void getAllCustomersTest() throws Exception {
        MvcResult getResult = restMvc.perform(get("/customers").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().is(HttpStatus.SC_OK)).andReturn();

        List<Customer> customers = (List<Customer>) testUtils.generateResponse(getResult);
        Assert.assertTrue(customers.size() > 0);
    }
}
