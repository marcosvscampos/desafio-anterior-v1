package br.com.builders.treinamento.utils;

import br.com.builders.treinamento.domain.Customer;
import br.com.builders.treinamento.dto.response.CustomerResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.springframework.test.web.servlet.MvcResult;

import java.util.List;

public class TestUtils {

    public Object generateResponse(MvcResult result) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        Object json = new JSONTokener (result.getResponse().getContentAsString()).nextValue();
        if(json instanceof JSONObject){
            CustomerResponse cr = mapper.readValue(result.getResponse().getContentAsString(), CustomerResponse.class);
            return cr;
        } else if (json instanceof JSONArray){
            List<Customer> customers = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<Customer>>(){});
            return customers;
        }
        return null;
    }
}
